@extends('layouts.app')
@section('content')

<div class="flex flex-col">



  @if ($message = Session::get('success'))
    <div class="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800" role="alert">
      {{ $message }}
    </div>
  @endif
  <div>Owner List </div> <div class="text-right"><a class="p-1 text-green-500" href="{{ route('owner.create') }}"><i class="fa fa-plus"></i> ADD</a></div>
  
  <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
    <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
      <div class="overflow-hidden">
      @if(count($items))
        <table class="min-w-full">
          <thead class="border-b">
            <tr>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                #
              </th>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                First Name
              </th>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                Last Name
              </th>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                Phone 
              </th>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                Action 
              </th>
            </tr>
          </thead>
          <tbody>
          
          @foreach ($items as $key => $item)
            <tr class="border-b">
              <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{{++$i}}</td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
              {{$item->first_name	}}
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
              {{$item->last_name	}}
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
              {{$item->phone	}}
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap flex">
              
                

                <a class="p-1 text-blue-500" href="{{ route('owner.edit',$item->id) }}"><i class="fa fa-edit"></i></a>
                <form action="{{ route('owner.destroy', $item->id) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!-- <button class="btn btn-danger m-1">Delete User</button> -->
                            <button class="p-1 text-red-500 show_confirm"><i class="fa fa-times"></i></button>
                </form>
              
              </td>
            </tr>
            @endforeach
           
            
          </tbody>
        </table>
        {!! $items->links() !!}
        @else
        NO Owner Found
        @endif
      </div>
    </div>
  </div>
</div>



	

@endsection