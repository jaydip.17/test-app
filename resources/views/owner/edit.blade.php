@extends('layouts.app')
@section('content')

<div class="w-full max-w-xs">
    {!! Form::model($item,['method' => 'PATCH','route' => ['owner.update', $item->id],'class' => 'bg-white shadow-2xl rounded px-8 pt-6 pb-8 mb-4']) !!}  
    <div class="text-xl">{{ __('Edit Owner') }}</div><br>
    <div class="mb-4">
    {{ Form::label('first_name', 'First Name', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::text('first_name',$item->first_name ,array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'first_name','placeholder'=>'First Name')) }}
    @error('first_name')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror
    </div>
    <div class="mb-4">
    {{ Form::label('last_name', 'Last Name', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::text('last_name',$item->last_name,array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'last_name','placeholder'=>'Last Name')) }}
    @error('last_name')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror  
    </div>
    <div class="mb-4">
    {{ Form::label('phone', 'Phone', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::text('phone',$item->phone,array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'phone','placeholder'=>'Phone')) }}
    @error('phone')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror  
    </div>
    <div class="flex items-center justify-between">
    {{Form::submit('Submit!', ['class' => 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'])}}  
    </div>
    {!! Form::close() !!}
</div>
@endsection