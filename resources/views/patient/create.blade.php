@extends('layouts.app')
@section('content')
<div class="w-full max-w-xs">
@if(count($owners))
{!! Form::open(['method' => 'POST','route' => ['patient.store'],'class' => 'bg-white shadow-2xl rounded px-8 pt-6 pb-8 mb-4']) !!}  
<div class="text-xl">{{ __('Create Patient') }}</div>
   <br>
<div class="mb-4">
    {{ Form::label('name', 'Name', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::text('name',NULL ,array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'name','placeholder'=>'Name')) }}
    @error('name')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror
    </div>
    <div class="mb-4">
    {{ Form::label('species', 'Species', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::text('species',NULL,array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'species','placeholder'=>'Species')) }}
    @error('species')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror  
    </div>
    <div class="mb-4">
    {{ Form::label('Color', 'Color', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::text('color',NULL,array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'color','placeholder'=>'Color')) }}
    @error('color')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror  
    </div>
    <div class="mb-4">
    {{ Form::label('date_of_birth', 'Date Of Birth', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::Date('date_of_birth',NULL,array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'date_of_birth','placeholder'=>'Date Of Birth')) }}
    @error('date_of_birth')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror  
    </div>
    <div class="mb-4">
    {{ Form::label('owner_id', 'Name Of Owner', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::select('owner_id', $owners,NULL,array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'owner_id','placeholder'=>'Select Owner')) }}
    @error('owner_id')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror  
    </div>
    <div class="flex items-center justify-between">
    {{Form::submit('Submit!', ['class' => 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'])}}  
    </div>
    {!! Form::close() !!}
    @else
    plese add at list one owner
    @endif
</div>
<script>
    var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0 so need to add 1 to make it 1!
var yyyy = today.getFullYear();
if(dd<10){
  dd='0'+dd
} 
if(mm<10){
  mm='0'+mm
} 

today = yyyy+'-'+mm+'-'+dd;
document.getElementById("date_of_birth").setAttribute("max", today);
</script>
@endsection