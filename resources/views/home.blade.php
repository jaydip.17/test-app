@extends('layouts.app')

@section('content')
<div class="w-full">
  <div class="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-5">
    <!--Card 1-->
    <div class="rounded overflow-hidden shadow-lg">
        <a href="{{ url('/home') }}">
        <div class="px-6 py-4">
          <div class="font-bold text-xl mb-2">Veterinarians</div>
          <p class="text-gray-700 text-base">
            {{$Veterinarians}}
          </p>
        </div>
      </a>
      </div>
    <!--Card 2-->
    <div class="rounded overflow-hidden shadow-lg">
      <a href="{{ url('owner') }}">
      
      <div class="px-6 py-4">
        <div class="font-bold text-xl mb-2">Owners</div>
        <p class="text-gray-700 text-base">
          {{$Owners}}
        </p>
      </div>
      
    </a>
    </div>
    <!--Card 3-->
    <div class="rounded overflow-hidden shadow-lg">
      <a href="{{ url('patient') }}">
      
      <div class="px-6 py-4">
        <div class="font-bold text-xl mb-2">Patients</div>
        <p class="text-gray-700 text-base">
          {{$patients}}
        </p>
      </a>
      </div>
    </div>
  </div>
</div>
</div>
@endsection