@extends('layouts.app')

@section('content')
<div class="w-full max-w-xs">
    {!! Form::open(['method' => 'POST','route' => ['register'],'class' => 'bg-white shadow-2xl rounded px-8 pt-6 pb-8 mb-4']) !!}  
    <div class="text-xl">{{ __('Veterinarian Register') }}</div>
    <br>
    @if(session()->has('error'))
          <div class="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-green-200 dark:text-green-800" role="alert">
          {{ session()->get('error') }}
          </div>
      @endif
    
    <div class="mb-4">
    {{ Form::label('email', 'E-Mail Address', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::text('email',NULL ,array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'email','placeholder'=>'E-Mail Address')) }}
    @error('email')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror
    </div>
    <div class="mb-4">
    {{ Form::label('password', 'Password', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::password('password',array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'password','placeholder'=>'Password')) }}
    @error('password')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror  
    </div>
    <div class="mb-4">
    {{ Form::label('password_confirmation', 'Confirm Password', array('class' =>'block text-gray-700 text-sm font-bold mb-2'))}}
    {{ Form::password('password_confirmation',array('class' =>'shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline','id'=>'password_confirmation','placeholder'=>'Confirm Password')) }}
    @error('password_confirmation')
    <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @enderror  
    </div>
    <div class="flex items-center justify-between">
    {{Form::submit('Login!', ['class' => 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'])}}  
    </div>
    {!! Form::close() !!}
</div>

<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection 