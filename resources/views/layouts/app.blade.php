<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	    <title>{{ config('app.name', 'Veterinarian') }}</title>
        <script src="https://cdn.tailwindcss.com"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	</head>
	<body >
		<nav class="bg-white shadow-lg">
			<div class="max-w-6xl mx-auto px-4">
				<div class="flex justify-between">
					<div class="flex space-x-7">
						<div>
							<!-- Website Logo -->
							<a href="{{ url('/') }}" class="flex items-center py-4 px-2">
							
								<span class="font-semibold text-gray-500 text-lg">{{ config('app.name', 'Laravel') }}</span>
							</a>
						</div>
						<!-- Primary Navbar items -->

                        @guest
                        @else
						<div class="hidden md:flex items-center space-x-1">
							<a href="{{ url('/home') }}" class="py-4 px-2  {{ Request::is('home*') ? 'text-green-500' : 'text-gray-500' }} font-semibold hover:text-green-500 transition duration-300">Home</a>
							<a href="{{ url('owner') }}" class="py-4 px-2 {{ Request::is('owner*') ? 'text-green-500' : 'text-gray-500' }} font-semibold hover:text-green-500 transition duration-300">Owner</a>
							<a href="{{ url('patient') }}" class="py-4 px-2 {{ Request::is('patient*') ? 'text-green-500' : 'text-gray-500' }} font-semibold hover:text-green-500 transition duration-300">Patient</a>
						</div>
                        @endif
					</div>
					<div class="hidden md:flex items-center space-x-3 ">
                        @guest
						<a href="{{ route('login') }}" class="py-2 px-2 font-medium {{ Request::is('login*') ? 'text-green-500' : 'text-gray-500' }} rounded hover:bg-green-500 hover:text-white transition duration-300">{{__('Login')}}</a>
						<a href="{{ route('register') }}" class="py-2 px-2 font-medium {{ Request::is('register*') ? 'text-green-500' : 'text-gray-500' }} rounded hover:bg-green-500 hover:text-white transition duration-300">{{__('Register')}}</a>
                        @else
						<a href="{{ route('logout') }}" class="py-2 px-2 font-medium {{ Request::is('logout*') ? 'text-green-500' : 'text-gray-500' }} rounded hover:bg-green-500 hover:text-white transition duration-300">{{__('Logout')}}</a>
                        @endif
                    </div>

					<!-- Mobile menu button -->
					<div class="md:hidden flex items-center">
						<button class="outline-none mobile-menu-button">
						<svg class="w-6 h-6 text-gray-500 hover:text-green-500 "
							x-show="!showMenu"
							fill="none"
							stroke-linecap="round"
							stroke-linejoin="round"
							stroke-width="2"
							viewBox="0 0 24 24"
							stroke="currentColor"
						>
							<path d="M4 6h16M4 12h16M4 18h16"></path>
						</svg>
					</button>
					</div>
				</div>
			</div>
			<!-- mobile menu -->
			<div class="hidden mobile-menu">
				<ul class="">
					@guest
                    <li><a href="{{url('/login')}}" class="block text-sm px-2 py-4 {{ Request::is('login*') ? 'text-green-500' : 'text-gray-500' }}  transition duration-300">{{__('Login')}}</a></li>
                    <li><a href="{{url('/register')}}" class="block text-sm px-2 py-4 {{ Request::is('register*') ? 'text-green-500' : 'text-gray-500' }}  transition duration-300">{{__('Register')}}</a></li>        
                    @else
                    <li><a href="{{url('/home')}}" class="block text-sm px-2 py-4 {{ Request::is('home*') ? 'text-green-500' : 'text-gray-500' }} transition duration-300">Home</a></li>
					<li><a href="{{url('/owner')}}" class="block text-sm px-2 py-4 {{ Request::is('owner*') ? 'text-green-500' : 'text-gray-500' }} transition duration-300">Owner</a></li>
					<li><a href="{{url('/patient')}}" class="block text-sm px-2 py-4 {{ Request::is('patient*') ? 'text-green-500' : 'text-gray-500' }} transition duration-300">Patient</a></li>
                    <li><a href="{{route('logout')}}" class="block text-sm px-2 py-4 {{ Request::is('logout*') ? 'text-green-500' : 'text-gray-500' }}  transition duration-300">{{__('Logout')}}</a></li>
                    @endif
				</ul>
			</div>
			<script>
				const btn = document.querySelector("button.mobile-menu-button");
				const menu = document.querySelector(".mobile-menu");

				btn.addEventListener("click", () => {
					menu.classList.toggle("hidden");
				});
			</script>
		</nav>
        <div class="pt-10 flex items-center justify-center">
      
                @yield('content')
            </div>
	</body>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript">
 
     $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Are you sure you want to delete this item?`,
              text: "If you delete this, it will be gone forever.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
  
</script>
</html>
