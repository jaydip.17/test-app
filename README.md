# test-app
## Deploy 

Clone the repository

    git clone https://gitlab.com/jaydip.17/test-app.git

Switch to the repo folder

    cd test-app

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate


Run the database migrations/seeders (**Set the database connection in .env before migrating**)

    php artisan migrate
    php artisan db:seed
Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone git@github.com:gothinkster/laravel-realworld-example-app.git
    cd test-app
    composer install
    cp .env.example .env
    php artisan key:generate
    php artisan migrate
    php artisan db:seed
    php artisan serve