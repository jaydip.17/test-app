<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Factories\HasFactory;

class Owner extends Model
{
    //
    // use HasFactory;
    protected $fillable = [
        'first_name',
        'last_name',
        'phone'
    ];
}
