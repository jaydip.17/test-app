<?php

namespace App\Http\Controllers;

use App\patient;
use App\Owner;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function index(Request $request)
    {
        $items = patient::orderBy('id','DESC')->paginate(5);
        return view('patient.table',compact('items'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $owners=Owner::Pluck('first_name','id')->toArray();
        return view('patient.create',compact('owners'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:25',
            'species'=>'required|max:25',
            'color'=>'required|max:25',
            'date_of_birth'=>'required|date',
            'owner_id'=>'required|exists:owners,id',
        ]);

        patient::create($request->all());

        return redirect()->route('patient.index')
                        ->with('success','Item created successfully');
    }

    public function edit(patient $patient)
    {
        $owners=Owner::Pluck('first_name','id')->toArray();
        $item = patient::find($patient->id);
        return view('patient.edit',compact('item','owners'));
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $this->validate($request, [
            'name'=>'required|max:25',
            'species'=>'required|max:25',
            'color'=>'required|max:25',
            'date_of_birth'=>'required|date',
            'owner_id'=>'required|exists:owners,id',
        ]);


        patient::find($id)->update($request->all());

        return redirect()->route('patient.index')
                        ->with('success','Item updated successfully');
    }

    public function destroy(patient $patient)
    {
        patient::find($patient->id)->delete();
        return redirect()->route('patient.index')
                        ->with('success','Item deleted successfully');

    }
}
