<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\patient;
use App\Owner;
use Auth;
use Hash;

class AuthController extends Controller
{
    public function register()
    {

      if (Auth::check()) {
        return redirect()->intended('home');
      }
      return view('auth.register');
      
    }

    public function storeUser(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);

        User::create([
            
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect('login')->with('success','Your account was successfully created.');
    }

    public function login()
    {

      if (Auth::check()) {
        return redirect()->intended('home');
    }

      return view('auth.login');
      
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('home');
        }

        return redirect('login')->with('error', 'Oppes! You have entered invalid credentials');
    }

    public function logout() {
      Auth::logout();

      return redirect('login');
    }

    public function home()
    {
      $Veterinarians=User::count();
      $patients=patient::count();
      $Owners=Owner::count();
      return view('home',compact('Veterinarians','patients','Owners'));
    }
}