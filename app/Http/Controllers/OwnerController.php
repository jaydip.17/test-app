<?php

namespace App\Http\Controllers;

use App\Owner;
use Illuminate\Http\Request;

class OwnerController extends Controller
{

    public function index(Request $request)
    {
        $items = Owner::orderBy('id','DESC')->paginate(5);
        return view('owner.table',compact('items'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('owner.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'=>'required|max:25',
            'last_name'=>'required|max:25',
            'phone'=>'required|max:10|min:10|unique:owners',
        ]);
        Owner::create($request->all());

        return redirect()->route('owner.index')
                        ->with('success','Item created successfully');
    }

    public function edit(Owner $owner)
    {
        $item = Owner::find($owner->id);
        return view('owner.edit',compact('item'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name'=>'required|max:25',
            'last_name'=>'required|max:25',
            'phone'=>'required|digits:10|unique:owners,phone,'.$id,
        ]);


        Owner::find($id)->update($request->all());

        return redirect()->route('owner.index')
                        ->with('success','Item updated successfully');
    }

    public function destroy(Owner $owner)
    {
        Owner::find($owner->id)->delete();
        return redirect()->route('owner.index')
                        ->with('success','Item deleted successfully');

    }
}
