<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Factories\HasFactory;

class patient extends Model
{
    //
    // use HasFactory;
    protected $fillable = [
        'name',
        'species',
        'color',
        'date_of_birth',
        'owner_id',
    ];
    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }
}
